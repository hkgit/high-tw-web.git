import './index.less'
import loading from './loading.gif'

const svg = (!!window.ActiveXObject || "ActiveXObject" in window) ?
  `
    <img src="${loading}" hight="40" width="40">
  ` : `
    <svg class="spinner spinner--circle" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
      <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
    </svg>
  `

function Loading() {
  this.length = 0
}

Loading.prototype.show = function() {
  this.length++
    if (this.length > 1) return
  document.getElementById('loading-dynamic').classList.add('show')
}

Loading.prototype.close = function() {
  if (this.length === 0) return
  this.length--
    if (this.length !== 0) return
  setTimeout(function() {
    document.getElementById('loading-dynamic').classList.remove('show')
  }, 300)
}

export default function(Vue) {
  let el = document.createElement('div')
  el.id = 'loading-dynamic'
  el.innerHTML = svg
  document.body.insertBefore(el, document.getElementById('app'))

  Vue.prototype.loading = new Loading()
}