export default function(Vue) {
  Vue.prototype.URL = {
    string(param, encode = true) {
      let paramStr = ""
      if (!param || typeof param !== 'object') return paramStr
      for (let key in param) {
        paramStr += "&" + key + "=" + (encode ? encodeURI(param[key]) : param[key])
      }
      return paramStr.substr(1)
    },
    parse(query, decode = true) {
      let param = {}
      if (!query || typeof query !== 'string') return param
      try {
        query.split('&').forEach(str => {
          param[str.split("=")[0]] = decode ? decodeURI(str.split("=")[1]) : str.split("=")[1]
        })
      } catch (e) {
        param = {}
      }
      return param
    }
  }
}