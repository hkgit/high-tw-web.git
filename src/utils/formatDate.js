const FormatDate = function(date, fmt) {
  if (!date) return ''
  if (typeof date === 'string') {
    date = new Date(date)
    if (isNaN(date.getTime())) return ''
  } else {
    if (!date.getTime) return ''
    if (isNaN(date.getTime())) return ''
  }
  if (/(Y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  let o = {
    'M+': date.getMonth() + 1,
    'D+': date.getDate(),
    'H+': date.getHours(),
    'm+': date.getMinutes(),
    's+': date.getSeconds()
  }
  for (let k in o) {
    if (new RegExp(`(${k})`).test(fmt)) {
      let str = o[k] + ''
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? str : padLeftZero(str))
    }
  }
  return fmt
}

function padLeftZero(str) {
  return ('00' + str).substr(str.length)
}

export default function(Vue) {
  Vue.prototype.formatDate = FormatDate
}

export { FormatDate }