import 'toastr/build/toastr.css'

import toastr from 'toastr'

export default function(Vue) {
	toastr.options = {
		"positionClass": "toast-top-center"
	}
  Vue.prototype.toastr = toastr
}