const alert = function(Vue, context, title = '') {
  return new Promise(resolve => {
    let el = null,
      alertconstructor = Vue.extend({
        template: `
        <b-modal visible title="${title}" centered size="sm" button-size="sm" title-tag="h6" ok-only ok-title="确定" @ok="hide(true)" @hidden="remove">
          ${context}
        </b-modal>
      `,
        methods: {
          hide(ev) {
            resolve(ev)
          },
          remove() {
            el.parentNode.removeChild(el)
          }
        }
      })
    const alertinstance = new alertconstructor().$mount()
    el = alertinstance.$el
    document.body.appendChild(alertinstance.$el)
  })
}

export default function(Vue) {
  Vue.prototype.alert = alert.bind(this, Vue)
}