const confirm = function(Vue, context, title = '') {
  return new Promise(resolve => {
    let el = null,
      confirmconstructor = Vue.extend({
        template: `
				<b-modal visible title="${title}" centered size="sm" button-size="sm" title-tag="h6" cancel-title="取消" ok-title="确定" @ok="hide(true)" @cancel="hide(false)" @hidden="remove">
			    ${context}
			  </b-modal>
			`,
        methods: {
          hide(ev) {
            resolve(ev)
          },
          remove() {
            el.parentNode.removeChild(el)
          }
        }
      })
    const confirminstance = new confirmconstructor().$mount()
    el = confirminstance.$el
    document.body.appendChild(el)
  })

}

export default function(Vue) {
  Vue.prototype.confirm = confirm.bind(this, Vue)
}