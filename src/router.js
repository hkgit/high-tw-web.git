import Vue from 'vue'
import Router from 'vue-router'
import Index from './demo/index'

Vue.use(Router)

export default new Router({
  routes: [{
    path: '',
    redirect: '/login'
  }, {
    path: '/login',
    name: 'login',
    component: Index
  }]
})