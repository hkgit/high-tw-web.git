import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Hightw from './index'

Vue.use(Hightw)

new Vue({
	router,
  render: h => h(App)
}).$mount('#app')
