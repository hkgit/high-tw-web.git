// 加载样式
// 样式的按需加载还没有解决方案
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'font-awesome/css/font-awesome.css'
import 'vue-multiselect/dist/vue-multiselect.min.css'
import 'vuescroll/dist/vuescroll.css'

// 加载bootstrap-vue组件库
import BootstrapVue from 'bootstrap-vue'
// 加载文件上传组件
import FileUpload from "vue-upload-component"
// 多选，搜索，搜索美化组件
import Multiselect from 'vue-multiselect'
// 侧边菜单栏组件
import Metismenu from './components/metismenu/metismenu'
// 报表组件，带搜索表单
import Reportform from './components/reportform'
// 滚动条组件
import Vuescroll from 'vuescroll'
// 时间日期选择组件
import Datepicker from 'vue-datepicker-local'
// 表单组件
import Richform from './components/richform'
// 表格组件，带搜索表单
import Tableform from './components/tableform'
// 树组件
import VJstree from 'vue-jstree'

// 加载提示工具
import Loading from './utils/loading/index'
// 加载网络请求工具
import Request from './utils/request'
// 加载提示工具
import Toastr from './utils/toastr'
// 确认框工具
import Confirm from './utils/confirm'
import Alert from './utils/alert'
// 日期格式工具
import FormatDateUtil, { FormatDate } from './utils/formatDate'
// url处理工具
import URL from './utils/URL'

export default function(Vue) {
	// 使用工具或组件
  Vue.use(BootstrapVue)
  Vue.use(Request)
  Vue.use(Toastr)
  Vue.use(Loading)
  Vue.use(Confirm)
  Vue.use(Alert)
  Vue.use(Vuescroll)
  Vue.use(FormatDateUtil)
  Vue.use(URL)

  // 注册单个组件，后期需要按需引入，而非全部集中引入
  Vue.component('fileupload', FileUpload)
  Vue.component('multiselect', Multiselect)
  Vue.component('metismenu', Metismenu)
  Vue.component('reportform', Reportform)
  Vue.component('datepicker', Datepicker)
  Vue.component('richform', Richform)
  Vue.component('tableform', Tableform)
  Vue.component('tree', VJstree)

  // 过滤器注册
  Vue.filter('formatDate', FormatDate)
}