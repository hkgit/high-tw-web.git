﻿# high-tw-web参考文档
------
## 主要技术参考
> * 系统框架，基于vue开发：[vue^2.5.17][1]
> * vue脚手架，快速搭建大型应用：[vue cli][2]
> * 样式文件LESS：[less^3.8.1][3]
> * 基础样式库：[bootstrap^4.1.1][4]
> * 基础组件库：[bootstrap-vue^2.0.0-rc.11][5]
> * 路由管理器，本系统使用hash模式：[vue-router^3.0.1][6]
> * 全局状态管理器：[vuex^3.0.1][7]
> * 字体图标库：[font-awesome^4.7.0][8]
> * 基于promise的HTTP库：[axios^0.18.0][9]
> * 消息提醒组件：[toastr^2.1.4][10]
> * 文件上传组件：[vue-upload-component^2.8.13][11]
> * 多功能下拉选择组件：[vue-multiselect^2.1.0][12]
> * 日期选择组件：[vue-datepicker-local^1.0.19][13]
> * 树组件：[vue-jstree^2.1.6][14]
> * 选择菜单组件：[metismenu^2.7.9][15]
> * 滚动条美化组件：[vuescroll^4.8.12][16]

------
## 组件库说明
此组件库全局加载了所有组件和插件，并封装了siderbar菜单栏组件、表单组件、报表组件、加载提示工具、alert\comfirm工具、消息提醒toastr工具、接口请求工具、url参数处理工具、日期格式化工具和过滤器
使用方法：
```
import Hightw from 'high-tw-web'
Vue.use(Hightw)
```
----------
## 开发环境搭建

 1. 编译器
    编译器推荐使用sublime，本文档将围绕sublime来进行说明
    下载地址：[Sublime Text 3][17]，选择对应平台下载安装  
 2. 编译器插件
    打开编译器，安住`ctrl+shift+p`，输入install，选择Install Package，再依次安装下面插件：
    LESS-sublime(高亮显示less文件)，Sublime-HTMLPrettify(格式化html、css、JavaScript、vue)
    **注意**：vue文件格式化需要修改Sublime-HTMLPrettify插件的默认设置，找到`global_file_rules>html>allowed_file_extensions`，在数组后面添加"vue"即可
 3. nodejs安装  
    下载地址：[nodejs][18]，选择对应平台下载安装，默认选项安装无需配置环境变量  
 4. npm配置  
    nodejs内置了npm，无需手动安装，了解npm请参考：[npm 中文文档][19]
    cnpm为淘宝npm镜像，可大大提高npm安装依赖的速度  
    cnpm临时使用：`npm --registry https://registry.npm.taobao.org install express`  
    cnpm持久使用：`npm config set registry https://registry.npm.taobao.org`  

------
## 组件库使用方法
### metismenu使用说明
```
<template>
<metismenu :menu="metismenu" :topLevel="topLevel" :field="field" style="width: 200px; height: 100%;" />
</template>
<script>
export default {
data() {
    return {
      topLevel: '', // 顶级菜单
      metismenu: [], // 菜单数据，无需转换成树结构，平级的
      field: {
          pid: 'PARENTCODE', // 指定pid为那个字段
          id: 'RESOURCECODE', // 指定id为那个字段
          name: 'RESOURCENAME', // 指定显示那个字段
          url: 'URL' // 指定跳转的url为那个字段
      }
    }
  }
}
</script>
```
### richform使用说明
```
<template>
<richform :form="form" @submit="submit" />
</template>
<script>
export default {
data() {
    return {
      form: [{
        name: 'input1', // 输入框的name，id（使用时，此输入框的值的key）；必填
        value: 'test', // 默认值，没有用空字符串
        label: '分中心', // 输入框的label名字
        inputCol: 2, // 输入框的宽度，默认占2格
        labelCol: 3, // 输入框label的宽度，默认占1格
        type: 'text', // 输入框的类型，支持：text,password,email,number,url,tel,range,color；
                      // 另外非input框支持：select,singleselect、multiselect、search,singlesearch、multisearch；
                      // time,date,datetime、radios、checkboxes、textarea、button、option
        required: true, // 是否必填，默认不必填，会阻止表单提交
        placeholder: '请输入', // 输入框的提示
        format(value) { // 格式化输入的值
          return value.toLowerCase() // 这里将输入的值转为小写，可转换逻辑自己实现
        },
        pattern: "[A-z]{3}", // 正则校验，不通过会阻止表单提交
        maxlength: 10, // 输入的最长长度
        max: 20, // 输入的最大值，仅限于type=number
        min: 10, // 输入的最小值，仅限于type=number
        msg: '输入错误的数据会提示', // 当表单校验失败后的提示
        hide: false, // 是否隐藏这个输入框和它的label，影藏后会占位
        disabled: false // 是否不可用
      }, {
        name: 'input5',
        value: new Date(), // 日期选择器的默认时间
        inputCol: 5,
        label: '开始时间',
        labelCol: 4,
        type: 'date', // type可以为time,date,datetime，都是日期选择控件
        required: false,
        placeholder: '请选择开始时间',
        config: {
          format: 'YYYY-MM-DD', // YYYY为年份选择，YYYY-MM为月份选择，YYYY-MM-DD为日期选择，YYYY-MM-DD HH:mm:ss为日期时间选择
                                // 输出的格式也是按照这个格式化后的结果
          minDate: new Date(), // 最小值
          maxDate_name: 'input6' // 最大值不操过name为input6的值
        }
      }, {
        name: 'input6',
        value: new Date(),
        inputCol: 5,
        label: '结束时间',
        labelCol: 1,
        type: 'date',
        required: false,
        placeholder: '请选择结束时间',
        config: {
          format: 'YYYY-MM-DD',
          minDate: new Date(),
          minDate_name: 'input5' // 最小值不超过name为input5的值
        }
      }, {
        name: 'input4',
        value: { // 默认选择的选项
          value: 2,
          text: '第二个'
        },
        inputCol: 2,
        label: '分中心',
        labelCol: 1,
        type: 'select', // 选择框，singleselect与select一样
        required: true,
        optionName: 'text', // 指定显示选择的选项的那个字段
        optionValue: 'value', // 指定选择的选项里面的那个字段为值，不指定时，取值时是选择的对象
        options: [{ // 下拉选项数据
          value: 1,
          text: '第一个第一个第一个第一个第一个第一个第一个第一个第一个第一个第一个第一个第一个第一个第一个第一个'
        }, {
          value: 2,
          text: '第二个'
        }, {
          value: 3,
          text: '第三个'
        }]
      }, {
        name: 'select1',
        value: [{ text: 'Adonis', language: 'JavaScript' }], // 多选框的默认值，为数组
        inputCol: 5,
        label: 'ssss',
        labelCol: 1,
        type: 'multiselect', // 多选框
        required: true,
        optionName: 'text',
        optionValue: 'language',
        placeholder: '请选择',
        options: [
          { text: 'Vue.jsVue.jsVue.jsVue.jsVue.jsVue.jsVue.jsVue.jsVue.jsVue.jsVue.jsVue.jsVue.js', language: 'JavaScript' },
          { text: 'Adonis', language: 'JavaScript' },
          { text: 'Rails', language: 'Ruby' },
          { text: 'Sinatra', language: 'Ruby' },
          { text: 'Laravel', language: 'PHP' },
          { text: 'Phoenix', language: 'Elixir' }
        ]
      }, {
        name: 'select2',
        value: '',
        inputCol: 5,
        label: 'select2',
        labelCol: 1,
        type: 'singlesearch', // 单选搜索框，和search一样
        required: true,
        optionName: 'text',
        placeholder: '请输入搜索',
        options: [
          { text: 'Vue.js', language: 'JavaScript' },
          { text: 'Adonis', language: 'JavaScript' },
          { text: 'Rails', language: 'Ruby' },
          { text: 'Sinatra', language: 'Ruby' },
          { text: 'Laravel', language: 'PHP' },
          { text: 'Phoenix', language: 'Elixir' }
        ]
      }, {
        name: 'select3',
        value: '',
        inputCol: 7,
        label: 'select3',
        labelCol: 1,
        type: 'search',
        required: false,
        optionName: 'managerInfo',
        placeholder: '请输入搜索',
        url: 'http://192.168.1.215:8089/hngs-web-dataReport-server/common/getAllBaseManagerName', // 选项数据从后台获取，此参数适用于任何选择
        param: { name: 'xxx', stationid: 3 }, // 获取选项数据的参数，此参数适用于任何选择
        likeSearch: false // 是否做输入搜索，适用于搜索选择
      }, {
        name: 'select4',
        value: [],
        inputCol: 4,
        label: 'select4',
        labelCol: 2,
        labelOffset: 1,
        type: 'multisearch',
        required: false,
        optionName: 'roadInfo',
        optionValue: 'roadId',
        placeholder: '请输入搜索',
        url: 'http://192.168.1.215:8089/hngs-web-dataReport-server/common/getBaseRoadByManager1',
        param: {},
        linkage: ['select3-managerId:managerId'] // 联动搜索选择框，此选择框会受name为select3的值的变化而重新查询此选择控件的选择数据，
                                                 // managerId为select3的选择的值，：后面的managerId为作为此选择控件的参数去后台查询，
                                                 // 一样时可以省略，写成linkage: ['select3-managerId']，有多个关联的，写在数组里面即可
      }, {
        name: 'radiostest',
        value: 2,
        inputCol: 3,
        label: '单选框',
        labelCol: 2,
        type: 'radios', // 单选框
        required: true,
        valueField: 'value1', // 指定取值的字段
        textField: 'text1', // 指定显示的字段
        radios: [{ // 单选的数据
          text1: '男',
          value1: 1
        }, {
          text1: '女',
          value1: 2
        }]
      }, {
        name: 'checkboxestest',
        value: [2], // 复选框value为数组
        inputCol: 2,
        label: '复选框',
        labelCol: 1,
        type: 'checkboxes', // 复选框
        // required: true,
        valueField: 'value1',
        textField: 'text1',
        checkboxes: [{
          text1: '第一个',
          value1: 1
        }, {
          text1: '第二个',
          value1: 2
        }]
      }, {
        name: 'textareatest',
        value: '',
        inputCol: 2,
        label: 'textareatest',
        labelCol: 1,
        type: 'textarea', // textarea
        rows: 4, // 默认3行
        maxRows: 10, // 最大行数，默认6行
        noResize: true, // 不可拉伸
        maxlength: 10000
      }, {
        // 布局用的，不需要type，一个占两行
        name: 'empty',
        inputCol: 1,
        labelCol: 1
      }, {
        name: 'button',
        type: 'button', // 按钮，默认会带查询和重置
        col: 3,
        align: 'right',
        // 不设置默认查询和重置
        buttons: [{
          variant: 'secondary', // 按钮颜色，bootstrap4的规范
          name: '自定义按钮',
          icon: 'thermometer-2', // 图标，fontawesome图标
          click: function() {
            alert('click')
          }
        }, {
          variant: 'secondary',
          name: '自定义按钮',
          click: function() {
            alert('click1')
          }
        }]
      }, {
        name: 'option',
        type: 'option', // 自定义功能按钮，可放任意位置，方便布局
        col: 3,
        align: 'center',
        // 不设置默认查询和重置
        options: [{
          variant: 'secondary',
          icon: 'university',
          name: '保存',
          click: function() {
            alert('click')
          }
        }]
      }]
    }
  },
  methods: {
    submit(param) {
      console.log(param) // 校验通过后会打印获取的表单值
    }
  }
}
/*
内置的方法有（通过ref调用）：
   initForm() 用户初始化表单，比如选择控件的option重新后后台获取
   search() 主动触发搜索，默认是点击查询触发搜索事件，也可以手动调用
   getSubmitData() 获取表单数据
   reset() 重置表单
*/
</script>
```
### reportform使用说明
```
<template>
<reportform :url="url" :form="form" @search="search" />
</template>
<script>
export default {
  data() {
    url: 'http://xxxxx?aaa=xxx', // 帆软报表url
    form: [] // 和richform一样
  },
  methods: {
    search(param) {
      console.log(param) // 好richform一样，具体业务处理，拼接url，并重新赋值url
    }
  },
}
</script>
```
### loading工具
```
this.loading.show() // 开启加载提示
this.loading.close() // 关闭加载提示
```
### alert、confirm工具
```
this.alert('context', 'title')
(async () => {
  const confirm = await this.confirm('context', 'title')
  if (!confirm) return
  // 业务代码
})()
```
### request工具，基于axios封装
```
/*
  url：接口地址，不带http会去.env里面配置的VUE_APP_BASE_URL上的值
  data：请求参数
  config：配置项
    loading：是否需要请求提示
    timeout：请求超时时间
    download：是否是数据流文件，就行下载
    cancel：取消请求的对象
      const source = axios.CancelToken.source()
      config.cancel = source
      使用：source.cancel('xxxxxx')
*/
(async () => {
  const url = '', data = {}, config = {}
  const result1 = await this.$post(url, data, config)
  const result2 = await this.$get(url, data, config)
  // 业务代码
})()

// 处理多个请求，统一得到结果
this.axios.all([this.$post(url, data, config), this.$post(url, data, config)]).then(this.axios.spread((res1, res2) => {
  // 业务代码
}))
```
### toastr提示工具
```
this.toastr.info('消息')
this.toastr.success('消息')
this.toastr.error('消息')
this.toastr.warning('消息')
```
### formatDate日期处理工具
```
<template>
{{ new Date() | formatDate('YYYY-MM-DD') }}
</template>
this.formatDate('2018-01-01', 'YYYY-MM-DD HH:mm:ss'))
```
### URL处理工具
```
const param = {
  a: '1',
  b: '2'
},
encode = true // 是否encodeURI
const str = this.URL.string(param, encode)
console.log(this.URL.parse(str))
```
  [1]: https://cn.vuejs.org/
  [2]: https://cli.vuejs.org/zh/
  [3]: http://lesscss.cn/
  [4]: https://getbootstrap.com/
  [5]: https://bootstrap-vue.js.org/
  [6]: https://router.vuejs.org/zh/
  [7]: https://vuex.vuejs.org/zh/
  [8]: http://www.fontawesome.com.cn/
  [9]: https://www.kancloud.cn/yunye/axios/234845
  [10]: https://github.com/CodeSeven/toastr
  [11]: https://github.com/lian-yue/vue-upload-component
  [12]: https://vue-multiselect.js.org/
  [13]: https://github.com/weifeiyue/vue-datepicker-local
  [14]: https://github.com/zdy1988/vue-jstree
  [15]: https://mm.onokumus.com/
  [16]: https://github.com/YvesCoding/vuescroll
  [17]: https://www.sublimetext.com/3
  [18]: http://nodejs.cn/download/
  [19]: https://www.npmjs.com.cn/